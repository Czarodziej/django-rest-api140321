from django.urls import path
from .views import ArticleAPIView, ArticleDetailsAIPView, ArticleGenericAPIView, ArticleDetailsGenericAPIView
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Api Basic endpoints')

urlpatterns = [
    path('article/', ArticleAPIView.as_view()),
    path('article/<str:author>', ArticleAPIView.as_view()),
    path('details/<int:id>', ArticleDetailsAIPView.as_view()),
    path('generic/article/', ArticleGenericAPIView.as_view()),
    path('generic/details/<int:id>', ArticleDetailsGenericAPIView.as_view()),
    path('swagger/', schema_view)
]

from django.contrib import admin
from .models import Article
from simple_history.admin import SimpleHistoryAdmin
from django.contrib.admin import site
import adminactions.actions as actions

admin.site.register(Article, SimpleHistoryAdmin)
actions.add_to_site(site)
